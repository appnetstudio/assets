let menuList = document.querySelectorAll(".btn-menu");
for (let i = 0; i < menuList.length; i++) {
    menuList[i].addEventListener("click", openMenu);
}

function openMenu(evt) {
    let list = document.querySelectorAll(".btn-menu");
    for (let i = 0; i < list.length; i++) {
        list[i].classList.remove('text-bold');
    }
    var btn = evt.target;
    btn.classList.add('text-bold');
    var id = btn.getAttribute('data-id');
    //
    list = document.querySelectorAll(".sample");
    for (let i = 0; i < list.length; i++) {
        list[i].classList.add('d-none');
    }
    let sample = document.getElementById(id);
    if(sample) {
        sample.classList.remove('d-none');
    }
}